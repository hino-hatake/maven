### Prerequisites
- `docker` 20.10+, `docker-compose` 1.29+
- `BuildKit` enabled, update your `/etc/docker/daemon.json`:
  ```json
  {
  "features": {
    "buildkit": true
    }
  }
  ```

### Docker build with BuildKit
First, build your cache image using multi-stage builds:
```bash
docker build --target build --cache-from hinorashi/maven:cache --tag hinorashi/maven:cache --build-arg BUILDKIT_INLINE_CACHE=1 .
```

Now build your main image:
```bash
docker-compose build
```

Push them to registry:
```bash
docker push hinorashi/maven:cache
docker push hinorashi/maven
```

Run it (kinda stupid :crocodile:):
```bash
docker-compose run maven
```
