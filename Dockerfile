# syntax=docker/dockerfile:1
FROM maven:3.6-jdk-8-alpine AS build

WORKDIR /code

#ENV MAVEN_OPTS="-Dmaven.repo.local=/code/.m2/repository \
#  -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN \
#  -Dorg.slf4j.simpleLogger.showDateTime=true \
#  -Djava.awt.headless=true"

COPY pom.xml /code/pom.xml
#RUN --mount=type=cache,target=/code/.m2 \
#  ["mvn", "dependency:resolve", "dependency:resolve-plugins"]
RUN ["mvn", "dependency:resolve", "dependency:resolve-plugins"]

# Adding source, compile and package into a fat jar
#COPY ["src/main", "/code/src/main"]
#RUN --mount=type=cache,target=/code/.m2 \
#  ["mvn", "install"]
RUN ["mvn", "install"]

FROM alpine:3.13

ARG STORAGE_LOCATION=/artifact

WORKDIR $STORAGE_LOCATION

#COPY --from=build /code/target/uploading-files-*.jar /app.jar
COPY --from=build /root/.m2/repository/one/hino/maven/1.0.0/maven-1.0.0.pom /artifact

VOLUME $STORAGE_LOCATION

CMD ["cat", "maven-1.0.0.pom"]

